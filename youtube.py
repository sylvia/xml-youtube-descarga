# -- Código para extraer información de un xml de Youtube.
# -- IMPORTS
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from urllib.request import urlopen
import sys

# -- HTML para poder visualizar el resultado
html = """
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Youtube Contents</title>
    </head>
    <body style="display: flex; flex-direction: column; align-items: center; justify-content: center; height: 100vh; background-color: #F0EEEE;">
        <img src="https://lh3.googleusercontent.com/4TR7dlSEhUdySTcbWWmSdTqgeoBkMntBcDw1LWqKlI9wSZOk1rqjcgFE6w1hyzFDbQM=w300" alt="Icono de Youtube" style="width: 100px; height: 100px; object-fit: contain; margin-top: 20px;">
        <h1 style="text-align: center;">Youtube Contents:</h1>
        <ul style="margin-top: 20px;">
            {videos}
        </ul>
    </body>
</html>
"""

# -- VARIABLES GLOBALES
videos = ''

# -- Clase que va a manejar la tarea que vamos a hacer.


class youtubeHandler(ContentHandler):

    def __init__(self):
        self.inEntry = False
        self.inContent = False
        self.link = ""
        self.titulo = ""
        self.contenido = ""

    def startElement(self, name, attrs):
        if name == 'entry':
            self.inEntry = True

        elif self.inEntry:
            if name == 'title':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')

    def endElement(self, name):

        global videos

        if name == 'entry':
            self.inEntry = False
            videos = videos + '<li><a href="' + self.link + '">' + self.titulo + '</a></li>'

        elif self.inEntry:
            if name == 'title':
                self.titulo = self.contenido
                self.contenido = ""
                self.inContent = False

    def characters(self, chars):
        if self.inContent:
            self.contenido = self.contenido + chars


def dowloadXmlYT(idYT):
    urlXml = "https://www.youtube.com/feeds/videos.xml?channel_id=" + idYT

    # -- Descargar el fichero xml
    xmlYTChannel = urlopen(urlXml)

    return xmlYTChannel

# -- Cargar el parser y el manejador
YTParser = make_parser()
YTHandler = youtubeHandler()
YTParser.setContentHandler(YTHandler)


# -- Programa principal
if __name__ == "__main__":

    if len(sys.argv) != 2:
        print("Usage: python youtube.py <document>")
        print(" ")
        sys.exit(1)

    # -- Aqui se abre el fichero xml
    xml = dowloadXmlYT(sys.argv[1])

    YTParser.parse(xml)
    paginaHtml = html.format(videos=videos)
    print(paginaHtml)
